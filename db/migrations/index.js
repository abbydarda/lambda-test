var AWS = require("aws-sdk");

AWS.config.update({
    region: "ap-southeast-1",
    endpoint: "http://localhost:8000"
});

const dynamodb = new AWS.DynamoDB();

async function createTable() {
    try {
        const params = {
            TableName: "todo_table_dev",
            KeySchema: [
                { AttributeName: "id", KeyType: "HASH" },  //Partition key
            ],
            AttributeDefinitions: [
                { AttributeName: "id", AttributeType: "S" }
            ],
            ProvisionedThroughput: {
                ReadCapacityUnits: 10,
                WriteCapacityUnits: 10
            }
        };

        await dynamodb.createTable(params).promise()
        console.log('migrasi berhasil');
    } catch (error) {
        console.log(error);
    }
}

createTable()