const docClient = require('../')
const {
    ScanCommand,
    GetItemCommand,
    PutItemCommand,
    UpdateItemCommand,
    DeleteItemCommand,
} = require("@aws-sdk/client-dynamodb");

async function listTodo(params) {
    try {
        let data = await docClient.send(new ScanCommand(params))
        return data
    } catch (err) {
        console.log(err);
        return err
    }
}

async function getId(params) {
    try {
        let data = await docClient.send(new GetItemCommand(params))

        return data
    } catch (err) {
        return err
    }
}

async function createTodo(params) {
    try {
        let data = await docClient.send(new PutItemCommand(params))
        return data
    } catch (err) {
        return err
    }
}

async function deleteTodo(params) {
    try {
        let data = await docClient.send(new DeleteItemCommand(params))
        return data
    } catch (err) {
        console.log(err);
        return err
    }
}

async function updateTodo(params) {
    try {
        let data = await docClient.send(new UpdateItemCommand(params))
        return data
    } catch (err) {
        return err
    }
}

async function search(params) {
    try {
        let data = await docClient.send(new ScanCommand(params))
        return data
    } catch (err) {
        return err
    }
}

module.exports = {
    listTodo,
    getId,
    createTodo,
    deleteTodo,
    updateTodo,
    search
}