# NodeJS | Lambda Local | Dynamodb Local

## Version
| Nama | Versi |
| ----------- | ----------- |
| Node | ```v16.13.1``` |
| npm | ```v8.3.0``` |
| lambda-local | ```2.0.0``` |
| java | `- openjdk 11.0.3 2019-04-16`<br> `- OpenJDK Runtime Environment (build 11.0.3+7-Ubuntu-1ubuntu218.10.1)` <br> `- OpenJDK 64-Bit Server VM (build 11.0.3+7-Ubuntu-1ubuntu218.10.1, mixed mode, sharing)` |



## Package
| Nama | Link |
| ----------- | ----------- |
| AWS SDK for JavaScript v3 | [AWS SDK for JavaScript v3](https://github.com/aws/aws-sdk-js-v3) |
| AWS SDK Util Dynamodb | [AWS SDK Util Dynamodb](https://www.npmjs.com/package/@aws-sdk/util-dynamodb) |
| AWS SDK Client Dynamodb | [AWS SDK Client Dynamodb](https://www.npmjs.com/package/@aws-sdk/client-dynamodb)
| DynamoDB local | [Dynamodb Local](https://docs.aws.amazon.com/id_id/amazondynamodb/latest/developerguide/DynamoDBLocal.DownloadingAndRunning.html) 


## Installation
install package
```bash
npm install
```

migrasi dynamodb
```
npm run dynamo:migration
```

menjalankan dynamodb local
```bash
java -Djava.library.path=./DynamoDBLocal_lib -jar DynamoDBLocal.jar -sharedDb
```

menjalankan unit testing
```bash
npm run test
```




