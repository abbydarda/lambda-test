const todos = require('../db/todos')
const { marshall, unmarshall } = require('@aws-sdk/util-dynamodb')

exports.handler = async (event, context) => {
    const { id } = event.pathParameters
    try {
        const params = {
            TableName: 'todo_table_dev',
            Key: marshall({ id: event.pathParameters.id }),
        }

        const data = await todos.deleteTodo(params)

        let response = {
            status: 1,
            info: "Data Berhasil dihapus",
        }

        return response
    } catch (err) {
        return { error: err }
    }
}