const todos = require('../db/todos')
const { marshall, unmarshall } = require('@aws-sdk/util-dynamodb')

exports.handler = async (event, context) => {
    let { search } = event.queryStringParameters

    try {
        const params = {
            TableName: 'todo_table_dev',
            FilterExpression: `contains(#key, :val)`,
            ExpressionAttributeValues: {
                ":val": { S: search }
            },
            ExpressionAttributeNames: {
                "#key": "name"
            },
        }

        const data = await todos.search(params)

        let response = {
            status: 1,
            info: "Data Berhasil diambil",
            data: data.Items.map(item => unmarshall(item)),
            count: data.Count,
            scannedCount: data.ScannedCount,
        }

        return response
    } catch (err) {
        console.log(err);
        return { error: err }
    }
}