const { v4: uuidv4 } = require('uuid');
const todos = require('../db/todos')
const { marshall, unmarshall } = require('@aws-sdk/util-dynamodb')

exports.handler = async (event, context) => {
    const { body } = event
    const id = uuidv4();
    body.id = id

    try {
        const params = {
            TableName: 'todo_table_dev',
            Item: marshall(body || {}),
        }
        const data = await todos.createTodo(params)

        let response = {
            status: 1,
            info: "Data Berhasil dibuat",
            id
        }

        return response
    } catch (err) {
        console.log(err);
        return { error: err }
    }
}