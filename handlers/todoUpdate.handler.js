const todos = require('../db/todos')
const { marshall, unmarshall } = require('@aws-sdk/util-dynamodb')

exports.handler = async (event, context) => {
    const { id } = event.pathParameters
    const { body } = event
    const objKey = Object.keys(body)

    const expression = `SET ${objKey.map((_, index) => `#key${index} = :value${index}`).join(",", "")}`

    const value = objKey.reduce((acc, key, index) => ({
        ...acc,
        [`:value${index}`]: body[key],
    }), {})

    const attr = objKey.reduce((acc, key, index) => ({
        ...acc,
        [`#key${index}`]: key,
    }), {})

    try {
        const params = {
            TableName: 'todo_table_dev',
            Key: marshall({ id: event.pathParameters.id }),
            UpdateExpression: expression,
            ExpressionAttributeValues: value,
            ExpressionAttributeNames: attr,
            ReturnValues: "UPDATED_NEW"
        }

        const data = await todos.updateTodo(params)

        let response = {
            status: 1,
            info: "Data Berhasil diubah",
        }

        return response
    } catch (err) {
        return { error: err }
    }
}