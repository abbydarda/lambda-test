const todos = require('../db/todos')
const { marshall, unmarshall } = require('@aws-sdk/util-dynamodb')

exports.handler = async (event, context) => {
    let pageSize = 10
    let lastItem
    if (event && event.queryStringParameters) {
        pageSize = event.queryStringParameters.pageSize
        lastItem = event.queryStringParameters.lastItem
    }

    try {
        const params = {
            TableName: 'todo_table_dev',
            Limit: pageSize
        }

        if (lastItem) {
            params.ExclusiveStartKey = { id: { S: lastItem } }
        }

        const data = await todos.listTodo(params)
        let response = {
            status: 1,
            info: "Data Berhasil diambil",
            data: data.Items.map(item => unmarshall(item)),
            count: data.Count,
            scannedCount: data.ScannedCount,
            lastItem: data.LastEvaluatedKey ? unmarshall(data.LastEvaluatedKey).id : null
        }

        return response
    } catch (err) {
        console.log(err);
        return { error: err }
    }
}