const todoCreate = require('./todoCreate')
const { mockClient } = require("aws-sdk-client-mock")
const { DynamoDBDocumentClient, GetCommand } = require('@aws-sdk/lib-dynamodb')
const { handler } = require("../handlers/todoGetId.handler")
const { DynamoDBClient } = require("@aws-sdk/client-dynamodb")
const client = new DynamoDBClient({
    region: 'localhost',
    endpoint: 'http://localhost:8000',
})

const ddbDocClient = DynamoDBDocumentClient.from(client)
const ddbMock = mockClient(ddbDocClient)
const variable = require('./variable.json')

async function getSpecEmpty() {
    describe("todo.getId", () => {
        beforeEach(() => {
            ddbMock.reset()
        })
        it("todo getId kosong", async () => {
            ddbMock.on(GetCommand).resolves(
                {
                    TableName: "todo_table_dev",
                    Key: variable.id
                }
            )

            const data = await handler(
                {
                    pathParameters: {
                        id: variable.id,
                    },
                }
            )
            await expect(data.data).toStrictEqual({})
        })
    })
}

async function getSpec() {
    describe("todo.getId", () => {
        beforeEach(() => {
            ddbMock.reset()
        })
        it("todo getId ada", async () => {
            ddbMock.on(GetCommand).resolves(
                {
                    Item: []
                }
            )
            const data = await handler(
                {
                    pathParameters: {
                        id: variable.id,
                    },
                }
            )

            await expect(data.data.id).toStrictEqual(variable.id)
        })
    })
}

module.exports = { getSpecEmpty, getSpec }