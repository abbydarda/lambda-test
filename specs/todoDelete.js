const { mockClient } = require("aws-sdk-client-mock")
const { DynamoDBDocumentClient, DeleteCommand } = require('@aws-sdk/lib-dynamodb')
const { handler } = require("../handlers/todoDelete.handler")
const { DynamoDBClient } = require("@aws-sdk/client-dynamodb")
const variable = require('./variable.json')

const client = new DynamoDBClient({
    region: 'localhost',
    endpoint: 'http://localhost:8000',
})

const ddbDocClient = DynamoDBDocumentClient.from(client)
const ddbMock = mockClient(ddbDocClient)

async function deleteSpec() {
    describe("todo.delete", () => {
        beforeEach(() => {
            ddbMock.reset()
        })

        it("delete Todo", async () => {
            ddbMock.on(DeleteCommand).resolves(
                {
                    TableName: "todo_table_dev",
                    Key: variable.id
                }
            )

            const data = await handler(
                {
                    pathParameters: {
                        id: variable.id,
                    },
                }
            )

            const data1 = await handler(
                {
                    pathParameters: {
                        id: variable.id1,
                    },
                }
            )

            const data2 = await handler(
                {
                    pathParameters: {
                        id: variable.id2,
                    },
                }
            )

            await expect(data.status).toBe(1)
        })

    })
}

module.exports = { deleteSpec }