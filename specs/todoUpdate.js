const todoCreate = require('./todoCreate')
const { mockClient } = require("aws-sdk-client-mock")
const { DynamoDBDocumentClient, UpdateCommand } = require('@aws-sdk/lib-dynamodb')
const { handler } = require("../handlers/todoUpdate.handler")
const { DynamoDBClient } = require("@aws-sdk/client-dynamodb")
const client = new DynamoDBClient({
    region: 'localhost',
    endpoint: 'http://localhost:8000',
})

const ddbDocClient = DynamoDBDocumentClient.from(client)
const ddbMock = mockClient(ddbDocClient)
const variable = require('./variable.json')

async function updateSpec() {
    describe("todo.update", () => {
        beforeEach(() => {
            ddbMock.reset()
        })
        it("todo update", async () => {
            ddbMock.on(UpdateCommand).resolves(
                {
                    TableName: 'todo_table_dev',
                    Key: variable.id,
                    Item: {
                        name: "mandi pagi",
                        status: 0
                    }
                }
            )
            const data = await handler(
                {
                    pathParameters: {
                        id: variable.id,
                    },
                    body: {
                        name: "mandi pagi",
                        status: 0
                    }
                }
            )

            await expect(data.status).toStrictEqual(1)
        })
    })
}

module.exports = { updateSpec }