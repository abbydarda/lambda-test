const { mockClient } = require("aws-sdk-client-mock")
const { handler } = require("../handlers/todoCreate.handler")
const variable = require('./variable.json')
const DocClient = require('../db');
const { DynamoDBDocumentClient, PutCommand } = require('@aws-sdk/lib-dynamodb')
const { DynamoDBClient } = require("@aws-sdk/client-dynamodb")

const client = new DynamoDBClient({
    region: 'localhost',
    endpoint: 'http://localhost:8000',
})

const ddbDocClient = DynamoDBDocumentClient.from(client)
const ddbMock = mockClient(ddbDocClient)

async function createSpec() {
    describe("todo.create", () => {
        beforeEach(() => {
            ddbMock.reset()
        })

        it("create Todo", async () => {

            ddbMock.on(PutCommand).resolves((
                {
                    TableName: "todo_table_dev",
                    Item: {
                        name: "mandi",
                        status: 0
                    }
                }
            ))

            const data = await handler(
                {
                    body: {
                        name: "mandi",
                        status: 0
                    }
                }
            )

            const data1 = await handler(
                {
                    body: {
                        name: "sarapan",
                        status: 0
                    }
                }
            )

            const data2 = await handler(
                {
                    body: {
                        name: "cuci piring",
                        status: 0
                    }
                }
            )

            variable.id = data.id
            variable.id1 = data1.id
            variable.id2 = data2.id

            await expect(data.status).toBe(1)
        })

    })
}

module.exports = { createSpec }