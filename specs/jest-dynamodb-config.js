module.exports = {
    tables: [
        {
            TableName: "todo_table_dev",
            KeySchema: [
                { AttributeName: "id", KeyType: "HASH" },  //Partition key
            ],
            AttributeDefinitions: [
                { AttributeName: "id", AttributeType: "S" }
            ],
            ProvisionedThroughput: {
                ReadCapacityUnits: 10,
                WriteCapacityUnits: 10
            }
        },
    ],
};
