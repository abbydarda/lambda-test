const { listSpecEmpty, listSpec, listSpecPagination1, listSpecPagination2 } = require('./todoList')
const { getSpecEmpty, getSpec } = require('./todoGetId')
const { createSpec } = require('./todoCreate')
const { deleteSpec } = require('./todoDelete')
const { searchSpec } = require('./todoSearch')
const { updateSpec } = require('./todoUpdate')

async function test() {
    listSpecEmpty()
    getSpecEmpty()
    createSpec()
    listSpec()
    listSpecPagination1()
    listSpecPagination2()
    getSpec()
    searchSpec()
    updateSpec()
    deleteSpec()
}

test()
