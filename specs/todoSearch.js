const todoCreate = require('./todoCreate')
const { mockClient } = require("aws-sdk-client-mock")
const { DynamoDBDocumentClient, UpdateCommand } = require('@aws-sdk/lib-dynamodb')
const { handler } = require("../handlers/todoSearch.handler")
const { DynamoDBClient, ScanCommand } = require("@aws-sdk/client-dynamodb")
const client = new DynamoDBClient({
    region: 'localhost',
    endpoint: 'http://localhost:8000',
})

const ddbDocClient = DynamoDBDocumentClient.from(client)
const ddbMock = mockClient(ddbDocClient)
const variable = require('./variable.json')

async function searchSpec() {
    describe("todo.search", () => {
        beforeEach(() => {
            ddbMock.reset()
        })
        it("todo search", async () => {
            ddbMock.on(ScanCommand).resolves(
                {
                    TableName: 'todo_table_dev',
                    FilterExpression: `contains(#key, :val)`,
                    ExpressionAttributeValues: {
                        ":val": { S: "ma" }
                    },
                    ExpressionAttributeNames: {
                        "#key": "name"
                    },
                }
            )
            const data = await handler(
                {
                    queryStringParameters: {
                        search: "ma"
                    }
                }
            )

            await expect(data.status).toStrictEqual(1)
        })
    })
}

module.exports = { searchSpec }