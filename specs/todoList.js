const todoCreate = require('./todoCreate')
const { mockClient } = require("aws-sdk-client-mock")
const { DynamoDBDocumentClient, ScanCommand } = require('@aws-sdk/lib-dynamodb')
const { handler } = require("../handlers/todoList.handler")
const { DynamoDBClient } = require("@aws-sdk/client-dynamodb")
const client = new DynamoDBClient({
    region: 'localhost',
    endpoint: 'http://localhost:8000',
})

const ddbDocClient = DynamoDBDocumentClient.from(client)
const ddbMock = mockClient(ddbDocClient)
const variable = require('./variable.json')

async function listSpecEmpty() {
    describe("todo.list", () => {
        beforeEach(() => {
            ddbMock.reset()
        })
        it("todo list kosong", async () => {
            ddbMock.on(ScanCommand).resolves(
                {
                    Item: []
                }
            )

            const data = await handler()

            await expect(data.data).toStrictEqual([])
        })
    })
}

async function listSpec() {
    describe("todo.list", () => {
        beforeEach(() => {
            ddbMock.reset()
        })
        it("todo list ada", async () => {
            ddbMock.on(ScanCommand).resolves(
                {
                    Item: []
                }
            )

            const data = await handler()
            await expect(data.status).toBe(1)
        })
    })
}

async function listSpecPagination1() {

    describe("todo.list", () => {

        beforeEach(() => {
            ddbMock.reset()
        })

        it("todo list pagination page 1", async () => {
            let pageSize = 2

            ddbMock.on(ScanCommand).resolves(
                {
                    TableName: "todo_table_dev",
                    Limit: pageSize
                }
            )


            const data = await handler(
                {
                    queryStringParameters: {
                        pageSize
                    }
                }
            )

            variable.lastItem = data.lastItem

            await expect(data.count).toStrictEqual(pageSize)
        })
    })
}

async function listSpecPagination2() {
    describe("todo.list", () => {

        beforeEach(() => {
            ddbMock.reset()
        })

        it("todo list pagination page 2", async () => {
            let pageSize = 2
            let lastItem = variable.lastItem

            ddbMock.on(ScanCommand).resolves(
                {
                    TableName: "todo_table_dev",
                    Limit: pageSize,
                    ExclusiveStartKey: lastItem,

                }
            )

            const data = await handler(
                {
                    queryStringParameters: {
                        pageSize,
                        lastItem,
                    }
                }
            )

            await expect(data.status).toStrictEqual(1)
        })
    })
}

module.exports = { listSpecEmpty, listSpec, listSpecPagination1, listSpecPagination2 }